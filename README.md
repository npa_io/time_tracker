# mitetracker

Mitetracker is a simple command line interface for mite (mite.yo.lk). The idea is to make it easy to register time in a software project without needing to visit the web UI.

The idea is that you should be able to start a tracker in any software project by typing ```mitetracker start```

# Setup

## 1: Create global .mitetracker.yml
The .mitetracker.yml file which should be stored in your home directory is where you specify your Mite API key. It is also here you setup the default service to the user when starting a tracker in a software project.

```
api_key: "YOURKEY"
default_service: Application development
```

## 2: Add a .mitetracker_local.yml to each project
The ```.mitetracker_local.yml``` is where you define the name of the project you're in. The local configuration makes it possible for mitetracker to set the project when starting a tracker automatically.

```
---
project_name: Project name
```

## 3: Start tracking
When in a project directory tree you can now type ```mitetracker start``` to have it starting tracking time. It will use the branch name as the note.
If the branch name starts with a number, it will prepend a hash to the note.

```
32-important-fix-for-emails -> #32 important fix for emails
another-branch-name -> another branch name
```

This is how mitetracker makes it easy to track anything.


# Favorites
Adding favorites to the global ```.mitetracker.yml``` file will make it easy for you to start the tracker on any project/service you use often.

```
api_key: "YOURKEY"
default_service: Application development
favorites:
  coffee:
    project: Breaks
    service: Coffee
  lunch:
    project: Breaks
    service: Lunch
```

After doing this, you're now able to write ```mitetracker coffee``` and ```mitetracker lunch```.
