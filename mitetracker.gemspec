Gem::Specification.new do |s|
  s.name        = "mitetracker"
  s.version     = "1.0.0"
  s.date        = "2016-12-04"
  s.summary     = "Commandline mite"
  s.description = "Time tracking with the mite service from the command line"
  s.authors     = ["Jakob Dam Jensen"]
  s.email       = "jakob@npapps.com"
  s.files       = [
    "lib/mitetracker.rb",
    "lib/mitetracker/config.rb",
    "lib/mitetracker/mite_initializer.rb",
    "lib/mitetracker/project.rb",
    "lib/mitetracker/service.rb",
    "lib/mitetracker/project_time_entry.rb",
    "lib/mitetracker/note.rb",
    "lib/mitetracker/git_utils.rb"
  ]
  s.homepage    = "http://rubygems.org/gems/mite_tracker"
  s.license     = "MIT"
  s.executables << "mitetracker"
end
