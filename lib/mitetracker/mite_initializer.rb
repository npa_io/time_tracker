require "mite-rb"
require "mitetracker/config"

Mite.account = "substancelab"
Mite.key = api_key

Mite.user_agent = "mite_cmd_tracker/1.0"
Mite.validate!
