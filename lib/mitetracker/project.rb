require "mitetracker/config"

def project
  project_with_name(project_name)
end

def project_with_name(project_name)
  @project ||= Mite::Project.all(:params => {:name => project_name}).first
  raise StandardError, "No project found" unless @project
  @project
end
