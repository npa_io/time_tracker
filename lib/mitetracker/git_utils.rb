def current_branch_name
  current_branch_name = `git rev-parse --abbrev-ref HEAD`
  current_branch_name.delete("\n")
end

def branch_name_contains_issue?
  /[0-9]+-.+/.match(current_branch_name)
end
