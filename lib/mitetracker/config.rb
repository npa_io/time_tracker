require "yaml"

def local_config_name
  ".mitetracker_local.yml"
end

def config_path
  directory_path = Dir.pwd
  while directory_path != "/" &&
      !File.exist?([directory_path, local_config_name].join("/"))
    directory_path = File.expand_path("..", directory_path)
  end
  raise StandardError, "project config not found" if directory_path == "/"

  [directory_path, local_config_name].join("/")
end

def config
  @config ||= YAML.load_file(config_path)
end

def global_config
  @global_config ||= YAML.load_file(File.expand_path("~/.mitetracker.yml"))
end

def api_key
  global_config["api_key"]
end

def project_name
  config["project_name"]
end

def store_tracking_id(id)
  config["tracking_id"] = id
  File.open(config_path, "w") { |f| YAML.dump(config, f) }
end

def stored_tracking_id
  config["tracking_id"]
end

def reset_stored_tracking_id
  store_tracking_id(nil)
end
