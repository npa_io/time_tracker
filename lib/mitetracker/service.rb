def service
  @service ||= service_with_name(global_config["default_service"])
end

def service_with_name(name)
  service = Mite::Service.
    all(:params => {
          :name => name
        }).
    first
  raise StandardError, "No service found" unless service
  service
end
