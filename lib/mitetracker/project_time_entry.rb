
def project_time_entry(project, tracker_id)
  @entry ||= project.
    time_entries.
    select { |entry| entry.id == tracker_id }.
    first
  raise StandardError, "No entry found" unless @entry
  @entry
end
