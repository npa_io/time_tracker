
def note
  if branch_name_contains_issue?
    issue_id = current_branch_name.split("-").first
    rest = (current_branch_name.split("-") - [issue_id]).join(" ")
    "##{issue_id} - #{rest}"
  else
    current_branch_name.tr("-", " ")
  end
end
