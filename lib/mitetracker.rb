require "mite-rb"
require "mitetracker/config"
require "mitetracker/mite_initializer.rb"
require "mitetracker/project"
require "mitetracker/service"
require "mitetracker/project_time_entry"
require "mitetracker/note"
require "mitetracker/git_utils"

def start_new_tracker
  @tracker ||= Mite::TimeEntry.create(
    :note => note,
    :project_id => project.id,
    :service_id => service.id
  )
  @tracker.start_tracker

  store_tracking_id(@tracker.id)

  puts "Started tracking for:\n#{project.name}: #{note}"

  @tracker
end

def start_favorite(favorite)
  favorite_info = global_config["favorites"][favorite]
  project = project_with_name(favorite_info["project"])
  service = service_with_name(favorite_info["service"])
  note = favorite_info["note"] || ""

  @tracker ||= Mite::TimeEntry.create(
    :note => note,
    :project_id => project.id,
    :service_id => service.id
  )
  @tracker.start_tracker

  store_tracking_id(@tracker.id)

  puts "Started tracking for:\n#{project.name}: #{service.name}"

  @tracker
end

def stop_tracker
  entry = project_time_entry(project, stored_tracking_id)
  entry.stop_tracker
  reset_stored_tracking_id
end
